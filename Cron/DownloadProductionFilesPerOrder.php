<?php
namespace Invition\InvitionZakekeM2\Cron;

class DownloadProductionFilesPerOrder {

	protected $_logger;			

    public function __construct(\Psr\Log\LoggerInterface $logger) {
        $this->_logger = $logger;
	}

    public function execute() {
        $this->_logger->info(__METHOD__);
		echo "cron send orders started \r\n";
		$target_dir = BP . "/";
		
		$lockfile = $target_dir . ".lockDownloadProductionFilesPerOrder";
		
		echo "check if lockfile exists<br/>\r\n";
		
		if (file_exists($lockfile)){
			unlink($lockfile);	
		}
		
		if (!file_exists($lockfile)) {
			fopen($lockfile, "w");			 		

			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();

			$model = $objectManager->create('Invition\InvitionZakekeM2\Model\Productupdater');
			$model->downloadZakekeDesignFiles();
			
			echo "remove lockfile<br/>\r\n";
	
			unlink($lockfile);	
		}

        return $this;
    }

}
