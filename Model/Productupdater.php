<?php 

namespace Invition\InvitionZakekeM2\Model;


class Productupdater extends \Magento\Framework\Model\AbstractModel 
{
	private $_objectManager;
	private $_productFactory;
	private $_productAttributeRepository;
	private $_productRepository;
	protected $_resource;
	private $_orderCollectionFactory;
	private $_transactionFactory;
	private $_trackFactory;
	private $_orderRepository;
	private $_statusbeforedownload;
	private $_statusafterdownload;
	 
	
	
	 /**
     * Initialize resource model
     *
     * @return void
     */
   	public function __construct(
		\Magento\Framework\ObjectManagerInterface $objectmanager,
		\Magento\Catalog\Model\ProductFactory $productFactory,		
		\Magento\Catalog\Model\Product\Attribute\Repository $productAttributeRepository,
		\Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
		\Magento\Framework\App\ResourceConnection $resource,		
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Framework\DB\TransactionFactory $transactionFactory,
        \Magento\Sales\Model\Order\Shipment\TrackFactory $trackFactory,
		\Magento\Sales\Api\OrderRepositoryInterface $orderRepository
	){
    	$this->_objectManager = $objectmanager;
		$this->_productFactory = $productFactory;
		$this->_productAttributeRepository = $productAttributeRepository;
		$this->_productRepository = $productRepository;				
		$this->_resource = $resource;
		$this->_orderCollectionFactory = $orderCollectionFactory;
		$this->_transactionFactory = $transactionFactory;
        $this->_trackFactory = $trackFactory;
		$this->_orderRepository = $orderRepository;
		
		$scopeConfig =  $this->_objectManager->create('\Magento\Framework\App\Config\ScopeConfigInterface');

	 	$this->_statusbeforedownload =   $scopeConfig->getValue('invition_invitionzakekem2/invitionzakekem2_orderstatussettings/invitionzakekem2_statusbeforedownload', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$this->_statusafterdownload =   $scopeConfig->getValue('invition_invitionzakekem2/invitionzakekem2_orderstatussettings/invitionzakekem2_statusafterdownload', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    } 
	

	/**
	 * 
	 * Walk through all orders with the right order status and download the images
	 * 
	 * @return void
	 */
	public function downloadZakekeDesignFiles() {
		$orders = $this->_orderCollectionFactory->create()->addAttributeToSelect('*')->addFieldToFilter('status', $this->_statusbeforedownload);
		
		foreach ($orders as $order) {
			if ($this->isZakekeOrder($order)) {
				$this->downloadOrderFilesFromOrder($order);								
			}	
		}
	}
	
	 
	/**
	 * 
	 * Check if an order is a Zakeke order
	 * 
	 * @param object $order
	 * @return bool
	 */
	public function isZakekeOrder($order) {
		$products =	$order->getAllItems();
		 
		foreach($products as $item)
		{
			$productoptions = $item->getProductOptions();	
			$additionaloptions = $productoptions["additional_options"];
			if (isset($additionaloptions)) {
				$additionaloptions = $additionaloptions[0];
				if ($additionaloptions["is_zakeke"]) {
					return true;
				}
			}
		}
		
		return false;
	}

	
	/**
	 * 
	 * Download the printfile from Zakeke
	 * 
	 * @param string	$url		Location of the file
	 * @param string	$filename	Filename of the new downloaded file
	 * @return void
	 * 
	 */
	public function downloadZakekeFile($url, $filename) {
		$fp = fopen($filename, 'w+');
		$ch = curl_init(str_replace(" ","%20",$url));

		// write curl response to file
		curl_setopt($ch, CURLOPT_FILE, $fp); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

		// get curl response
		curl_exec($ch); 
		curl_close($ch);
		fclose($fp);
	}
	

	/**
	 * 
	 * Extract the downloaded zipfile to specified folder
	 * 
	 * @param string 	$zipfile	Name of the downloaded zipfile
	 * @param string	$zipfolder	Location of the downloaded zipfile
	 * @return string	Link to extracted file location
	 */
	public function extractZakekeFile($zipfile, $zipfolder) {
		$result = "";
		
		$zip = new \ZipArchive;
		$res = $zip->open($zipfile);
		if ($res === TRUE) {
			$zip->extractTo($zipfolder);
			$zip->close();		  
			
		  
			$dh = opendir($zipfolder); 
			while (($file = readdir($dh)) !== false) { 
				if($file !== '.' && $file !== '..') {
					$path_parts = pathinfo($file); // php 5.2
					 
					if (array_key_exists("extension", $path_parts)) {
						if (strtoupper($path_parts['extension']) == "PNG") {
							$result = $zipfolder . $file;
							
						} else {
							unlink($zipfolder . $file);
						}
					}
				}
			}
		}
		
		unlink($zipfile);
		
		return $result;
	}
	
	/**
	 * 
	 * Connect the downloaded Zakeke design file to the order
	 * 
	 * @param object $order
	 * @param string $filename	Name of the file to connect
	 * @param string $fordesign
	 * @return void
	 */
	public function addDesignToOrder($order, $filename, $fordesign) {
		$orderlines = $order->getAllItems();
		
		foreach($orderlines as $orderline) {
			$productOptions = $orderline->getProductOptions();
	 
			
			if (!isset($productOptions["downloadedproductionfile"])) {
				
				$additionaloptions = $productOptions["additional_options"];					 						
						
				if (isset($additionaloptions)) {
					$additionaloptions = $additionaloptions[0];
					
					if ($additionaloptions["is_zakeke"]) {
						$design = $additionaloptions["design"];

						if ($design == $fordesign) {

							if (file_exists($filename)) {
					
								$productOptions["downloadedproductionfile"] = $filename;

								$orderline->setProductOptions($productOptions);
								$orderline->save();
							}
						}
					}
				}
			}		 
		}
	}
	
	/**
	 * 
	 * Download the design files for specific order
	 * 
	 * @param object $order
	 * @return void
	*/
	public function downloadOrderFilesFromOrder($order) {
	
		// Walkthrough order lines to see which lines require a design download
		foreach($order->getAllItems() as $item) {
			$productoptions = $item->getProductOptions();	
			$additionaloptions = $productoptions["additional_options"];
			if (isset($additionaloptions)) {
				$additionaloptions = $additionaloptions[0];
				if ($additionaloptions["is_zakeke"]) {
					$design 		= $additionaloptions["design"];
					
					$zakekehelper =  $this->_objectManager->create('\Futurenext\Zakeke\Helper\ZakekeApi\ZakekeApi');
					
				 	$zipUrl 		= $zakekehelper->getZakekeOutputZip($design);
					
					if (!file_exists(BP . "/var/production/png/")) {
						mkdir(BP . "/var/production/png", 0777, true);
					}
					
					$filename 		= BP . "/var/production/png/" . $design . ".zip";
					$zipfolder 		= BP . "/var/production/png/" . $design . "/";
					
					$this->downloadZakekeFile($zipUrl, $filename);
					
					$designfilename = $this->extractZakekeFile($filename, $zipfolder);
				 
					$this->addDesignToOrder($order, $designfilename, $design);
				}
			}			
		}
		
		// If the images are downloaded, change the status and add a comment
		if ($this->allImagesDownloaded($order)) {
			$history = $order->addStatusHistoryComment('Zakeke printfiles downloaded.', false);
			$history->setIsCustomerNotified(false);
 
			$order->save();
			
			$order->setData('state', 'processing');
			$order->setStatus($this->_statusafterdownload);    	
			$order->save();	
		}
	}
	
	/**
	 * 
	 * Are all the neccessary design files downloaded?
	 * 
	 * @param object $order
	 * @return bool
	 */
	public function allImagesDownloaded($order){

		$orderlines = $order->getAllItems();
		
		foreach($orderlines as $orderline) {
			$productOptions = $orderline->getProductOptions();
			$additionaloptions = $productOptions["additional_options"];		
			if (isset($additionaloptions)) {
				$additionaloptions = $additionaloptions[0];

				if ($additionaloptions["is_zakeke"]) {
					if (!isset($productOptions["downloadedproductionfile"])) {
						return false;
					}
				}
			
			}
		}

		return true;

	}

}
