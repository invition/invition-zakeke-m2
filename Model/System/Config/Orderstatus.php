<?php
namespace Invition\InvitionZakekeM2\Model\System\Config;

class Orderstatus extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource {

    protected $eavEntityFactory;
	protected $statusCollectionFactory;

    public function __construct(
        \Magento\Eav\Model\EntityFactory $eavEntityFactory,
		\Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory $statusCollectionFactory
    ) {
        $this->eavEntityFactory = $eavEntityFactory;
		$this->statusCollectionFactory = $statusCollectionFactory;
    }
    public function getAllOptions()
    {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		
		$result = array();
		
		return $result;
    }
 
    public function toOptionArray()
    {
        $options = $this->statusCollectionFactory->create()->toOptionArray();        
        return $options;
    }
	
 	private function addItem($label) {
		$item = array();

		$item["value"] = $label;
		$item["label"] = $label;			

		return $item;
	}
    
}
